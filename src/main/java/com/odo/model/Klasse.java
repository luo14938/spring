package com.odo.model;

import javax.persistence.*;

@Entity
public class Klasse {

	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.AUTO) // f�r autoincrement
	private int k_id;
	@Column
	private String k_name;
	@Column
	private int anzahl;
	public Klasse (){};
	public Klasse(int k_id, String k_name, int anzahl) {
		super();
		this.k_id = k_id;
		this.k_name = k_name;
		this.anzahl = anzahl;
	}
	public int getK_id() {
		return k_id;
	}
	public void setK_id(int k_id) {
		this.k_id = k_id;
	}
	public String getK_name() {
		return k_name;
	}
	public void setK_name(String k_name) {
		this.k_name = k_name;
	}
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
}
