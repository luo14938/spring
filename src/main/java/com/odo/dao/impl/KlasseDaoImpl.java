package com.odo.dao.impl;

import java.util.List;

import com.odo.dao.*;
import com.odo.model.Klasse;
import org.hibernate.SessionFactory;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Repository;  

@Repository
public class KlasseDaoImpl implements KlasseDao {

	@Autowired
	private SessionFactory session;
	
	@Override
	public void add(Klasse klasse) {
		// TODO Auto-generated method stub
		session.getCurrentSession().save(klasse);
	}

	@Override
	public void edit(Klasse klasse) {
		// TODO Auto-generated method stub
		session.getCurrentSession().update(klasse);
	}

	@Override
	public void delete(int k_id) {
		// TODO Auto-generated method stub
		
		session.getCurrentSession().delete(getKlasse(k_id));
	}

	@Override
	public Klasse getKlasse(int k_id) {
		// TODO Auto-generated method stub
		return (Klasse) session.getCurrentSession().get(Klasse.class, k_id);
	}

	@Override
	public List getKlassen() {
		// TODO Auto-generated method stub
		return session.getCurrentSession().createQuery("from k_klassen").list();
	}

}
