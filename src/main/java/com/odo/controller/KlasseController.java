package com.odo.controller;

import java.util.Map;

import javax.xml.ws.RequestWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.odo.model.Klasse;
import com.odo.service.KlasseService;

@Controller
public class KlasseController {
	
	@Autowired
	private KlasseService klasseservice;
	
	@RequestMapping("/index")
	public String setupForm(Map<String, Object> map){
		Klasse klasse = new Klasse();
		map.put("k_klasse", klasse);
		map.put("Klassen", klasseservice.getKlassen());
		return "k_klasse";
	}
	
	@RequestMapping(value="/klasse.do",method=RequestMethod.POST)
	public String doActions(@ModelAttribute Klasse klasse, BindingResult result, @RequestParam String action,Map<String, Object> map){
		Klasse klasseR = new Klasse();
		switch(action.toLowerCase()) {
			case "add":
				klasseservice.add(klasse);
				klasseR=klasse;
				break;
			case "edit":
				klasseservice.edit(klasse);
				klasseR=klasse;
				break;
			case "delete":
				klasseservice.delete(klasse.getK_id());
				klasseR=new Klasse();
				break;
			case "search":
				Klasse gesuchteKlasse =klasseservice.getKlasse(klasse.getK_id());
				klasseR=gesuchteKlasse != null ? gesuchteKlasse : new Klasse();
				break;
		}
		
		map.put("Klasse",klasseR);
		map.put("Klassenliste",klasseservice.getKlassen());
		return "klasse";
	}
}
