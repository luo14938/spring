package com.odo.dao;

import java.util.List;

import com.odo.model.Klasse;

public interface KlasseDao {

	public void add(Klasse klasse);
	public void edit(Klasse klasse);
	public void delete(int k_id);
	public Klasse getKlasse(int k_id);
	public List getKlassen();

}
