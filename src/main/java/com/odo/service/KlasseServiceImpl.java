package com.odo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.odo.dao.KlasseDao;
import com.odo.model.Klasse;

@Service
public class KlasseServiceImpl implements KlasseService {
	
	@Autowired
	private KlasseDao klasseDao;

	@Transactional
	public void add(Klasse klasse) {
		klasseDao.add(klasse);

	}

	@Transactional
	public void edit(Klasse klasse) {
		klasseDao.edit(klasse);
	}

	@Transactional
	public void delete(int k_id) {
		klasseDao.delete(k_id);
	}

	@Transactional
	public Klasse getKlasse(int k_id) {
		// TODO Auto-generated method stub
		return klasseDao.getKlasse(k_id);
	}

	@Transactional
	public List getKlassen() {
		// TODO Auto-generated method stub
		return klasseDao.getKlassen();
	}

}
