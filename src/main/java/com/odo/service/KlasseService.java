package com.odo.service;

import java.util.List;

import com.odo.model.Klasse;

public interface KlasseService {

	public void add(Klasse klasse);
	public void edit(Klasse klasse);
	public void delete(int k_id);
	public Klasse getKlasse(int k_id);
	public List getKlassen();

}
